const pool = require("../lib/db")

async function countLikes(){
    const [rows] = pool.query(`SELECT id,count(picture_id) 
                                FROM pictures
                                LEFT JOIN likes on pictures.id=likes.picture_id 
                                GROUP BY picture_id;`)
    return rows
}