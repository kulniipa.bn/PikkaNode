const getDetailPhotoHandler = (ctx) => {
	ctx.body = 'detail page ' + ctx.params.id
}

module.exports = {
	getDetailPhotoHandler
}
