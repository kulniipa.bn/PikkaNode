'use strict'

const path = require('path')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')

const pool = require('../lib/db')

const pictureDir = path.join(__dirname, 'public/images')
console.log("pictureDir : ",pictureDir)
const allowFileType = {
    'image/png': true,
    'image/jpeg': true
}
const uploadHandler = async ctx => {
    try {
        // check allow file type
        if (!allowFileType[ctx.request.files.photo.type]) {
            throw new Error('file type not allow')
        }
        // form data & file data
        console.log(ctx.request.body.caption)
        // file data
        console.log(ctx.request.files.photo.name)
        console.log(ctx.request.files.photo.path)

        let photoName = ctx.request.files.photo.name
        let photoPath = ctx.request.files.photo.path
        let caption = ctx.request.body.caption
        let userId = ctx.session.userId
        console.log("User ID from upload file : ",ctx.session.userId)
        const fileName = uuidv4() // generate uuid for file name
        //insert to database
        await setDetailPhoto(fileName,caption,userId)
        //move uploaded file from temp dir to destination
        await fs.copy(ctx.request.files.photo.path, path.join(pictureDir, fileName))
        ctx.redirect('/')
    } catch (err) {
        // handle error here
        ctx.status = 400
        ctx.body = err.message
        // remove uploaded temporary file when the error occurs
        fs.remove(ctx.request.files.photo.path)
    }
}

async function setDetailPhoto(fileName,caption){
    await pool.query(`INSERT INTO pictures(id,caption,created_by) VALUES (?,?,?)`,[fileName,caption,userId])
}

module.exports = {
    uploadHandler
}