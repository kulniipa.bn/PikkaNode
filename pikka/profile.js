const pool = require("../lib/db")

const profileGetHandler = async (ctx) => {
    console.log(ctx.session.userId)
    let strSearch;
    let txtSearch = ctx.query.search
    if (txtSearch == undefined) {
        strSearch = "caption like '%%'"
    } else {
        strSearch = "caption like '%" + txtSearch + "%'"
    }
    console.log(ctx.query.search)
    let filename = await getFilename(strSearch)
    let obj = {}
    obj.details = filename
    await ctx.render('home', obj)
}

async function getFilename(srtSearch) {
    let [rows] = await pool.query("SELECT id,caption,created_at FROM pictures WHERE " + srtSearch)
    return rows
}

module.exports = { profileGetHandler }