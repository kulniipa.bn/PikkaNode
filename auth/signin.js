'use strict'
const bcrypt = require('bcrypt')
const pool = require('../lib/db')

async function signinGetHandler(ctx) {
    const data = {
        flash: ctx.flash
    }
    console.log(data)
    await ctx.render('signin', data)
}

async function signinPostHandler(ctx) {
    //get data from accessToken Facebook API
    /*let accessToken = ctx.request.body.fb_token
    if(accessToken!=='' || accessToken!==undefined){
        let userID = ctx.request.body.fb_user_id
        let emailFB = ctx.request.body.fb_profile.email
        let nameFB = ctx.request.body.fb_profile.name
        const findUser = await getUserFacebook(userID)
        if(findUser.length === 0){
            await setUserFacebook(userID,emailFB)            
        }else{
            await updateUsrFacebook(userID,emailFB,nameFB)
        }
        
        ctx.session.userId = userID
        
        //return ctx.redirect('/profile')
    }*/

    //console.log(ctx.request.body)
    let email = ctx.request.body.email
    let password = ctx.request.body.password
    const hashedPassword = await getUserDetail(email)
    const userId = await getUserDetail(email)

    let countPassword = hashedPassword.length
    if (countPassword > 0) {
        const comparePassword = await bcrypt.compare(password, hashedPassword[0].password)
        if (!comparePassword) {
            ctx.session.flash = {
                error: 'wrong password'
            }
        } else {
            ctx.session.flash = {
                error: 'login successfuly'
            }
            ctx.session.userId = userId[0].id
            console.log(ctx.session)
            return ctx.redirect('/')
        }
    } else {
        ctx.session.flash = {
            error: 'no user'
        }
    }
    //return ctx.redirect('/')
    ctx.redirect('/signin')
}
async function setUserFacebook(fbUserId,username){
    await pool.query(`INSERT INTO user(facebook_user_id,username) VALUES(?,?)`,[fbUserId,username])

}
async function getUserFacebook(fbUserId){
    const [rows] = await pool.query(`SELECT * FROM user WHERE facebook_user_id=?`,[fbUserId])
    return rows
}
async function updateUsrFacebook(fbUserId,username,firstname){
    await pool.query(`UPDATE user SET firstname=?,username=? WHERE facebook_user_id=?`,[firstname,username,fbUserId])
}
async function getUserDetail(email) {
    const [rows] = await pool.query(`SELECT * FROM users WHERE email=?`, [email])
    return rows
}

module.exports = {
    signinGetHandler,
    signinPostHandler
}