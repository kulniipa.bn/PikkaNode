'use strict'
const pool = require('../lib/db')
const bcrypt = require('bcrypt')


async function signupGetHandler(ctx) {
    await ctx.render('signup')
}

async function signupPostHandler(ctx) {
    let email = ctx.request.body.email
    let password = ctx.request.body.password
    let hashedPassword = await bcrypt.hash(password,10)
    console.log(hashedPassword)

    try {
        await setUserPassword(email, hashedPassword)
        ctx.redirect('/signin')
    } catch (err) {
        ctx.status = 400
        ctx.body = err.message
        console.log(err)
        
        if (err.errno === 1062) {
            ctx.body = "e-mail duplicate!"
        } else {
            ctx.body = "Please contact admin!"
        }
    }
}

async function setUserPassword(email, hashedPassword) {
    const result = await pool.query(`INSERT INTO users(email,password) VALUES (?,?)`, [email, hashedPassword])
    console.log(result)
}

module.exports = {
    signupGetHandler,
    signupPostHandler
}

