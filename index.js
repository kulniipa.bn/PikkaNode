'use strict'

const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const render = require('koa-ejs')
const path = require('path')
const koaBody = require('koa-body')
const session = require('koa-session')
const cors = require('@koa/cors')
//upload file
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')

const pool = require('./lib/db')
const signin = require('./auth/signin')
const signup = require('./auth/signup')
//const upload = require('./pikka/upload')
const profile = require('./pikka/profile')
const detail = require('./pikka/detail')

const app = new Koa()
const router = new Router()

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'template',
  viewExt: 'ejs',
  cache: false
})

router.get('/signin',checkSignin,signin.signinGetHandler)
router.post('/signin',signin.signinPostHandler)
router.get('/signup',checkSignin,signup.signupGetHandler)
router.post('/signup',signup.signupPostHandler)

//upload photo
const pictureDir = path.join(__dirname, 'public/images')
const allowFileType = {
  'image/png': true,
  'image/jpeg': true
}
const uploadHandler = async ctx => {
  try {
    // check allow file type
    if (!allowFileType[ctx.request.files.photo.type]) {
      throw new Error('file type not allow')
    }
    let photoName = ctx.request.files.photo.name
    let photoPath = ctx.request.files.photo.path
    let caption = ctx.request.body.caption
    let userId = ctx.session.userId

    const fileName = uuidv4()
    await setCaptionPhoto(fileName, caption,userId)
    // move uploaded file from temp dir to destination
    await fs.copy(ctx.request.files.photo.path, path.join(pictureDir, fileName))
    
    ctx.redirect('/')
  } catch (err) {
    ctx.status = 400
    ctx.body = err.message
    fs.remove(ctx.request.files.photo.path)
  }
}

async function setCaptionPhoto(fileName, caption,userId) {
  await pool.query(`INSERT INTO pictures(id,caption,created_by) VALUES (?,?,?)`, [fileName, caption, userId])
}

async function getFilename(srtSearch) {
  let [rows] = await pool.query(`SELECT u.email,DATE_FORMAT(p.created_at,'%d-%M-%Y %H:%s:%m') AS created_at,
                                  p.id AS picture_id,p.caption
                                  FROM users AS u
                                  INNER JOIN pictures AS p ON u.id=p.created_by WHERE ${srtSearch}`)
  return rows
}

//check authen
async function checkSignin (ctx, next){
  if (ctx.session.userId != undefined) {
    return ctx.redirect('/')
  }
  await next()
}

async function checkAuth (ctx, next){
  if (!ctx.session || !ctx.session.userId) {
    return ctx.redirect('/signin')
  }else{
    return ctx.redirect('/')
  }
  await next()
}
router.get('/profile', checkAuth, profile.profileGetHandler)

router.get('/', async ctx => {
  console.log(ctx.session.userId)
  let strSearch
  let txtSearch = ctx.query.search
  if (txtSearch == undefined) {
    strSearch = "caption like '%%'"
  } else {
    strSearch = "caption like '%" + txtSearch + "%'"
  }
  let filename = await getFilename(strSearch)
  let obj = {}
  obj.details = filename
  await ctx.render('home', obj)
})

router.get('/create', async ctx => {
  await ctx.render('upload')
})

router.post('/create', koaBody({
  multipart: true
}), uploadHandler)

//comment
router.post('/pikka/:id/comment',async ctx =>{
  //check like
  let like = await getLike(ctx.session.userId,ctx.params.id)
  let imgLikeUnlike
  if(like.length!==0){
    imgLikeUnlike = "like-icon.png"
  }else{
    imgLikeUnlike = "unlike-icon.png"
  }

  //comment
  let id = ctx.params.id
  let userId = ctx.session.userId
  if(ctx.request.body.comment!==''){
    await setComment(ctx.request.body.comment,ctx.params.id,userId)
  }

  let photoId = await getPhotoDetails(ctx.params.id)
  let comments = await getComments(ctx.params.id)
  let objComment = {}
  objComment.comments = await getComments(ctx.params.id)
  let objId = {}
  objId.picture = await getPhotoDetails(ctx.params.id)
  let data = {picture:objId.picture,comments:objComment.comments,pictureId:id,imgLikeUnlike:imgLikeUnlike}

  await ctx.render('feed',data)
})

async function setComment(strComment,pictureId,userId){
  const [rows] = await pool.query(`INSERT INTO comments (text,picture_id,created_by) VALUES (?,?,?)`,[strComment,pictureId,userId])
  return rows  
}

//likes and unlike
router.post('/pikka/:id/like',async ctx => {
  let like = await getLike(ctx.session.userId,ctx.params.id)
  let imgLikeUnlike
  if(like.length!==0){
    await unlike(ctx.session.userId,ctx.params.id)
    imgLikeUnlike = "unlike-icon.png"
  }else{
    await setLike(ctx.session.userId,ctx.params.id)
    imgLikeUnlike = "like-icon.png"
  }
  
  let id = ctx.params.id
  let photoId = await getPhotoDetails(ctx.params.id)
  let comments = await getComments(ctx.params.id)
  let objComment = {}
  objComment.comments = await getComments(ctx.params.id)
  let objId = {}
  objId.picture = await getPhotoDetails(ctx.params.id)
  let data = {picture:objId.picture,comments:objComment.comments,pictureId:id,imgLikeUnlike:imgLikeUnlike}

  await ctx.render('feed',data)
})
async function setLike(userId,pictureId){
  const [rows] = await pool.query(`INSERT INTO likes (user_id,picture_id) VALUES (?,?)`,[userId,pictureId])
  return rows  
}
async function getLike(userId,pictureId){
  const [rows] = await pool.query(`SELECT * FROM likes WHERE user_id=${userId} AND picture_id='${pictureId}'`)
  return rows  
}
async function unlike(userId,pictureId){
  const [rows] = await pool.query(`DELETE FROM likes WHERE user_id=${userId} AND picture_id='${pictureId}'`)
  return rows  
}

//pikka/:id
router.get('/pikka/:id', async ctx => {
  let like = await getLike(ctx.session.userId,ctx.params.id)
  let imgLikeUnlike
  if(like.length!==0){
    imgLikeUnlike = "like-icon.png"
  }else{
    imgLikeUnlike = "unlike-icon.png"
  }

  let id = ctx.params.id;
  const photoId = await getPhotoDetails(ctx.params.id)
  const comments = await getComments(ctx.params.id)
  const objComment = {}
  objComment.comments = await getComments(ctx.params.id)
  let objId = {}
  objId.picture = await getPhotoDetails(ctx.params.id)
  let data = {picture:objId.picture,comments:objComment.comments,pictureId:id,imgLikeUnlike:imgLikeUnlike}
  await ctx.render('feed',data)
})

async function getPhotoDetails(photoId){
  const [rows] = await pool.query(`select users.email as posted_by,DATE_FORMAT(pictures.created_at,'%d-%M-%Y %H:%s:%m') as created_at, 
                                    pictures.id as picture_id,pictures.caption
                                    from users
                                    inner join pictures on users.id=pictures.created_by where pictures.id=?`,[photoId])
  return rows
}
async function getComments(photoId){
  const [rows] = await pool.query(`select * from comments where picture_id=?`,[photoId])
  return rows
}

//session
const sessionStore = {}
const sessionConfig = {
  key: 'sess',
  maxAge: 1000 * 60 * 60,
  httpOnly: true,
  store: {
    get(key, maxAge, {
      rolling
    }) {
      return sessionStore[key]
    },
    set(key, sess, maxAge, {
      rolling
    }) {
      sessionStore[key] = sess
    },
    destroy(key) {
      delete sessionStore[key]
    }
  }
}
//flash message
const flash = async (ctx, next) => { // Flash middleware
  if (!ctx.session) throw new Error('flash message required session')
  ctx.flash = ctx.session.flash
  delete ctx.session.flash
  await next()
}

app.use(cors({
  origin: 'https://panotza.github.io',
  allowMethods: ['GET', 'POST'],
  allowHeaders: ['Authorization', 'Content-Type'],
  maxAge: 5,
  credentials: true
}))
app.keys = ['supersecret']
app.use(session(sessionConfig, app))
app.use(flash)
app.use(koaBody())
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(4000)